FROM ubuntu:18.04

RUN apt-get update \
	&& apt-get install -y \
		curl openssh-server git vim mc unzip locales sudo mc \
	&& apt-get autoremove -y \
	&& apt-get clean -y \
	&& rm -rf /var/lib/apt/lists/*

RUN sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config \
	&& mkdir -p \
		/var/run/sshd \
		/app \
		/home/dev/.ssh \
		/home/dev/.vscode-server \
	&& touch /home/dev/.gitconfig

# dev user

RUN useradd --system -rm -d /home/dev -s /bin/bash -g root -G sudo -u 1000 dev

COPY .git-completion.bash .yarn-completion.bash /home/dev/

RUN echo "source /home/dev/.git-completion.bash" >> /home/dev/.bashrc \
	&& echo "source /home/dev/.yarn-completion.bash" >> /home/dev/.bashrc

RUN chown -R dev /home/dev /app

RUN echo "dev  ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# env

ENV SHELL /bin/bash
ENV EDITOR=vim

# sshd


EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
